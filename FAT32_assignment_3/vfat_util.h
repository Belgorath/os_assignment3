#ifndef __VFAT_UTIL_H__
#define __VFAT_UTIL_H__

#define FUSE_USE_VERSION 26

#include <stdlib.h>
#include <string.h>
#include <fuse.h>
#include "vfat.h"



int vfat_is_dirname_valid(char *dirname, int N);
void vfat_parse_attr(struct fat32_direntry *direntry, struct stat *st);
unsigned char checksum(struct fat32_direntry *direntry);
#endif
