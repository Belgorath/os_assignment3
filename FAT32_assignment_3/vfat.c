#define FUSE_USE_VERSION 26
#define _GNU_SOURCE

#include <sys/mman.h>
#include <assert.h>

#ifdef __APPLE__
#include <machine/endian.h>
#else
#include <endian.h>
#endif

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <fuse.h>
#include <iconv.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <signal.h>

#include "vfat.h"
#include "vfat_util.h"
#include "bytebuffer.h"

#define DEBUG_PRINT(A, ...) fprintf(stderr, "[DEBUG] (%d:%s): " A "\n", __LINE__,\
                                    __FILE__, ##__VA_ARGS__)
#define err_unless(C, M, ...) \
    if(!(C)) {err(1, "[FATAL ERROR] (%d:%s): " M "\n", __LINE__, __FILE__, \
                  ##__VA_ARGS__);}
#define mem_test(M) err_unless(M, "Memory allocation error.")
#define BYTE unsigned char


struct vfat_data vfat_info;
iconv_t iconv_utf16;

uid_t mount_uid;
gid_t mount_gid;
time_t mount_time;

/* Used for debug
static void PRINT_BYTES(char *id, BYTE* buff, size_t nbytes)
{
    fprintf(stderr, "%s:", id);
    int i;
    for (i = 0; i < nbytes; ++i) {
        fprintf(stderr, " %02X", buff[i]);
    }
    fprintf(stderr, "\n");
}*/

static inline off_t vfat_sector_to_bytes(off_t sectors)
{
    return ((off_t) vfat_info.boot_sector.bytes_per_sector) *
        sectors;
}

static void vfat_test_boot_sector(struct fat_boot *boot_sector)
{
    /* Check the validity of the jump instruction BS_jmpBoot. */
    if (boot_sector->jmp_boot[0] == 0xEB) {
        err_unless(boot_sector->jmp_boot[2] == 0x90,
                   "Invalid pattern in BS_jumpBoot of boot sector. "
                   "BPB[2] should be 0x90 if BPB[0]=0xEB but was %02X.",
                   boot_sector->jmp_boot[2]);
    } else {
        err_unless(boot_sector->jmp_boot[0] == 0xE9,
                   "Invalid first BPB byte. Expected: 0xEB or 0xE9"
                   " but was %02X.", boot_sector->jmp_boot[0]);
    }

    /* The BS_OEMName is too vague in the spec to be considered. */

    /* Checking the BPB_BytsPerSec (the number of bytes per sector). */
    uint16_t num_bytes_per_sector = boot_sector->bytes_per_sector;
    err_unless(num_bytes_per_sector == 512 || num_bytes_per_sector == 1024 ||
               num_bytes_per_sector == 2048 || num_bytes_per_sector == 4096,
               "BPB_BytsPerSec (# of bytes per sectore) must be "
               "512, 1024, 2048 or 4096 but was %d.", num_bytes_per_sector);

    /* Checking the BPB_SecPerClus (the # of sector per cluster). */
    uint8_t num_sectors_per_cluster = boot_sector->sectors_per_cluster;
    err_unless(num_sectors_per_cluster == 1 || num_sectors_per_cluster == 2 ||
               num_sectors_per_cluster == 4 || num_sectors_per_cluster == 8 ||
               num_sectors_per_cluster == 16 || num_sectors_per_cluster == 32 ||
               num_sectors_per_cluster == 64 || num_sectors_per_cluster == 128,
               "The legal values for BPB_SecPerClus are powers of two"
               " greater than 0 and at most 128. Given: %d.",
               num_sectors_per_cluster);

    err_unless(((int) num_bytes_per_sector) *
               ((int)num_sectors_per_cluster) <= 32768,
               "(BPB_BytsPerSec * BPB_SecPerClus) is greater than 32K.");

    /* Checking the BPB_RsvdSecCnt (the # of reserved bytes). */
    err_unless(boot_sector->reserved_sectors > 0, "The number of reserved "
               "sector must be positive.");

    /* Checking the BPB_NumFATs (the # of file allocation table). */
    err_unless(boot_sector->fat_count == 2, "The number of FAT must be 2.");

    /* Checking the BPB_RootEntCnt. */
    err_unless(boot_sector->root_max_entries == 0, "The BPB_RootEntCnt is "
               "non-zero.");

    /* Checking the BPB_TotSec16. */
    err_unless(boot_sector->total_sectors_small == 0, "The BPB_TotSec16 is "
               "non-zero.");

    /* Checking the BPB_Media. */
    uint8_t  media_info = boot_sector->media_info;
    err_unless(media_info == 0xF0 || media_info == 0xF8 || media_info == 0xF9 ||
               media_info == 0xFA || media_info == 0xFB || media_info == 0xFC ||
               media_info == 0xFD || media_info == 0xFE || media_info == 0xFF,
               "BPB_Media has unlegal value, i.e. %d.", media_info);

    /* Checking the BPB_FATSz16. */
    err_unless(boot_sector->sectors_per_fat_small == 0,
               "BPB_FATSz16 is non-zero.");

    /* No need to check BPB_SecPerTrk, BPB_NumHeads, BPB_HiddSec */

    /* Checking the BPB_TotSec32. */
    err_unless(boot_sector->total_sectors > 0, "BPB_TotSec32 is 0 or less.");

    /* No need to check BPB_FATSz32, BPB_ExtFlags */

    /* Checking the BPB_FSVer (the file system version) */
    err_unless(boot_sector->fat32.version == VFAT_FAT32_FS_VERSION,
               "FAT32 version is non-zero.");

    /* The BPB_RootClus is too vague to be checked. */

    /* No need to check BPB_FSInfo, BPB_BkBootSec, BPB_Reserved, ... */

}

static void vfat_check_fat_type(struct fat_boot *boot_sector)
{
    /* Implementation of the type checking algorithm described
     * in the Microsoft specifications. */
    int root_dir_sectors = (32*((int) boot_sector->root_max_entries) +
        ((int) boot_sector->bytes_per_sector) - 1) /
        ((int) boot_sector->bytes_per_sector);

    int sectors_per_fat = (boot_sector->sectors_per_fat_small != 0) ?
        boot_sector->sectors_per_fat_small :
        boot_sector->fat32.sectors_per_fat;

    int total_sectors = (boot_sector->total_sectors_small != 0) ?
        boot_sector->total_sectors_small :
        boot_sector->total_sectors;

    int data_sectors = total_sectors - ((int) boot_sector->reserved_sectors) -
        (((int) boot_sector->fat_count)*((int) sectors_per_fat)) +
        root_dir_sectors;

    int cluster_count = data_sectors / ((int) boot_sector->sectors_per_cluster);

    char *err_mess = "The FS of the volume is %s; only FAT32 supported";
    if (cluster_count < 4085) err(1, err_mess, "FAT12");
    else if (cluster_count < 65525) err(1, err_mess, "FAT16");
}

static void vfat_init(const char *dev)
{
    iconv_utf16 = iconv_open("UTF-8", "UTF-16LE"); /* from utf-16 to utf-8 */
    err_unless(iconv_utf16 != (iconv_t) -1, "Cannot convert a filename in UTF-8.");
    
    /* These are useful so that we can setup correct permissions
     *in the mounted directories */
    mount_uid = getuid();
    mount_gid = getgid();

    /* Use mount time as mtime and ctime for the filesystem
     * root entry (e.g. "/") */
    mount_time = time(NULL);

    vfat_info.fs = open(dev, O_RDONLY);
    if (vfat_info.fs < 0)
        err(1, "open(%s)", dev);

    /* Read the first 512 bytes of the device */
    BYTE buffer[512];
    read(vfat_info.fs, buffer, 512);

    /* Then we "parse" the content of the BPB */
    memcpy(&(vfat_info.boot_sector), buffer, sizeof(struct fat_boot));

    vfat_test_boot_sector(&(vfat_info.boot_sector));
    vfat_check_fat_type(&(vfat_info.boot_sector));
}

static int vfat_is_valid_cluster(uint32_t index_cluster)
{
    uint32_t masked_index = (index_cluster & 0x0fffffff);
    
    if (masked_index >= (uint32_t) 0x0ffffff8) {
        return 0;
    } else if (masked_index == (uint32_t) 0x0ffffff7) {
        return 0;
    }
    
    return 1;
}

/** Sets the position indicator at the beginning of the FATs */
static inline void vfat_seek_to_fat(off_t entry_index)
{
    entry_index *= 4;
    lseek(vfat_info.fs,
          vfat_sector_to_bytes(vfat_info.boot_sector.reserved_sectors) +
          entry_index,
          SEEK_SET);
}

/** Sets the position indicator at the beginning of the
 *  cluster_index-th cluster */
static inline void vfat_seek_to_cluster(off_t cluster_index)
{
    off_t fat_size =
        vfat_sector_to_bytes(vfat_info.boot_sector.fat32.sectors_per_fat);
    off_t num_fat = vfat_info.boot_sector.fat_count;
    off_t num_res_bytes =
        vfat_sector_to_bytes(vfat_info.boot_sector.reserved_sectors);

    cluster_index -= 2;
    cluster_index *=
        vfat_sector_to_bytes(vfat_info.boot_sector.sectors_per_cluster);

    off_t offset = num_res_bytes + (num_fat * fat_size) +
        cluster_index;

    lseek(vfat_info.fs, offset, SEEK_SET);
}

/** Returns the index of the next cluster given the current one.
 *  Be careful this function does not restor the disk head. **/
static uint32_t vfat_next_cluster(uint32_t current_cluster)
{
    uint32_t cluster;

    /* Seek to the right fat. */
    vfat_seek_to_fat(current_cluster);
    read(vfat_info.fs, &cluster, sizeof(uint32_t));

    return cluster;
}

/* Convert the raw long name entry into proper UTF-8 char seq. */
static inline size_t vfat_fill_buffer_with_lname(char *dst, uint16_t *src, size_t max)
{
    
    /* Find the number of character to convert. */
    size_t i;
    size_t count = 0;
    for (i = 0; i < max && src[i] != 0x0000 && src[i] != 0xffff; ++i) {
        count += 1;
    }
    
    if (count > 0) {
        char *src_ptr = (char*)(src);
        char *dst_prt = dst;
        size_t src_l = count * sizeof(uint16_t);
        size_t dst_l = count * sizeof(char);
        
        iconv(iconv_utf16, &(src_ptr), &src_l, &(dst_prt), &dst_l);
        
        
    }
    return count;
}

/** Helper function to deal with long names. It is given
 *  a destination buffer in which it writes its result.
 *  It is also given a bytebuffer containing the past longname
 *  entry that needs to be parsed. Note that the destination
 *  buffer is supposed to have a sufficient length. In any case,
 *  dst must contains 12 bytes but it can/must contain more depending
 *  on the number of long direntry encountered before. Returns 0 in
 *  case of error and 1 in case of success. */
static int vfat_getname(char *dst, struct fat32_direntry *sdirentry, bytebuffer_t *buf,
                        int num)
{
    if (bbuffer_is_empty(buf)) {
        /* In this case we parse a short name. */
        if (!vfat_is_dirname_valid(sdirentry->nameext, 11)) {
            if (num == 1) {
                dst[0] = '.';
                return 1;
            } else if (num == 2) {
                dst[0] = '.';
                dst[1] = '.';
                return 1;
            }
            return 0;
        }
        
        memset(dst, '\0', 11);
        
        int i = 0;
        int j = 0;
        int there_was_space = 0;
        for (i = 0; i < 11; ++i) {
            if ((uint8_t)(sdirentry->nameext[i]) != 0x20) {
                if (there_was_space) {
                    dst[j] = '.';
                    j++;
                    there_was_space = 0;
                }
                dst[j] = sdirentry->nameext[i];
                ++j;
            } else there_was_space = 1;
        }
    } else {
        /* In this cas we parse a longname. */
        size_t total_bytes = bbuffer_bytes(buf);
        memset(dst, 0, total_bytes);
        
        /* Iterates over the long entries preceding sdirentry. */
        struct fat32_direntry_long ldirentry;
        
        uint8_t t = 1;
        
        /* These indicators are used to control the format of the long direntry. */
        int seq_ok = 0;
        int done = 0;
        int seq_error = 1;
        int csum_error = 0;
        
        uint8_t csum;
        int first_csum_read = 0;
        
        char *dst_backup = dst;
        
        while (!bbuffer_is_empty(buf)) {
            
            buf = bbuffer_pop(buf, &ldirentry);
            seq_ok = 0;
            
            if (!done) {
                if (ldirentry.seq == t) {
                    seq_ok = 1;
                } else if ((ldirentry.seq & 0x3f) == t) {
                    seq_ok = 1;
                    done = 1;
                    seq_error = 0;
                } else {
                    seq_error = 1;
                    done = 1;
                }
                
                if (first_csum_read && csum != ldirentry.csum) {
                    csum_error = 1;
                }
                
                if (seq_ok) {
                    dst += vfat_fill_buffer_with_lname(dst, ldirentry.name1, 5);
                    dst += vfat_fill_buffer_with_lname(dst, ldirentry.name2, 6);
                    dst += vfat_fill_buffer_with_lname(dst, ldirentry.name3, 2);
                }
            } else {
                seq_error = 1;
            }
            
            ++t;
            
            first_csum_read = 1;
            csum = ldirentry.csum;
        }
        
        if (checksum(sdirentry) != csum) {
            csum_error = 1;
        }
        
        if (seq_error || csum_error) {
            vfat_getname(dst_backup, sdirentry, NULL, num);
        }
    }
    
    return 1;
}

/** Reads the directory starting at first_cluster-th cluster. */
static void vfat_readdir(uint32_t first_cluster, fuse_fill_dir_t filler,
                         void *fillerdata)
{
    struct stat st;
    struct fat32_direntry direntry;
    char *name_dst;
    bytebuffer_t *long_name_buffer = NULL;
    
    memset(&st, 0, sizeof(st));
    memset(&direntry, 0, sizeof(direntry));
    
    st.st_uid = mount_uid;
    st.st_gid = mount_gid;
    
    /* Seeks to the start of the cluster. */
    vfat_seek_to_cluster(first_cluster);
    
    uint32_t cluster_size =
    ((uint32_t) vfat_info.boot_sector.sectors_per_cluster) *
    ((uint32_t) vfat_info.boot_sector.bytes_per_sector);
    
    /* The following loop reads the entry one after the other. */
    read(vfat_info.fs, &direntry, sizeof(struct fat32_direntry));
    uint32_t num_read_entries = 1;
    
    /* The loop stops whenever we reach an entry starting by a zero byte or
     * when we are at the end of the cluster. */
    uint8_t first_direntry_byte = direntry.name[0];
    
    
    int end_of_dir = 0;
    
    while (first_direntry_byte != 0x00 && !end_of_dir) {
        
        if (VFAT_IS_LONG_DIRENTRY(direntry)) {
            long_name_buffer = bbuffer_push(long_name_buffer, &direntry,
                                            sizeof(struct fat32_direntry_long));
        }
        else if (VFAT_IS_SHORT_DIRENTRY(direntry))
        {
            /* Calls the routine to parse the name (long or short). */
            
            if (!bbuffer_is_empty(long_name_buffer)) {
                name_dst = calloc(bbuffer_bytes(long_name_buffer), sizeof(char));
                mem_test(name_dst);
            } else {
                name_dst = calloc(12, sizeof(char));
                mem_test(name_dst);
            }
            
            
            if (vfat_getname(name_dst, &direntry, long_name_buffer, num_read_entries)) {
                vfat_parse_attr(&direntry, &st);
                if (!VFAT_IS_VOLUME(direntry)) filler(fillerdata, name_dst, &st, 0);
            }
            
            long_name_buffer = NULL;
            free(name_dst);
        }
        
        /* If we are at the end of a cluster and need to read the next one. */
        if ((num_read_entries * sizeof(direntry)) >= cluster_size)
        {
            /* Compute the next cluster index. */
            uint32_t next_cluster = vfat_next_cluster(first_cluster);
            
            /* If the next cluster is ok, seek to it. */
            if (vfat_is_valid_cluster(next_cluster)) {
                vfat_seek_to_cluster(next_cluster);
                first_cluster = next_cluster;
                num_read_entries = 0;
            } else {
                end_of_dir = 1;
            }
        }
        
        /* Reads the next entry. */
        if (!end_of_dir) {
            read(vfat_info.fs, &direntry, sizeof(struct fat32_direntry));
            first_direntry_byte = direntry.name[0];
            ++num_read_entries;
        }
    }
    
    
}


static int _vfat_seek_to_path(const char* path, uint32_t from_cluster)
{
    err_unless(path, "vfat_seek_to_path is given NULL as a path.");
    char *name_dst;
    bytebuffer_t *long_name_buffer = NULL;
    
    /* Move in the path after the first char */
    char *after_slash = strstr(path, "/") + 1;
    
    if (after_slash != NULL || strlen(after_slash) != 0) {
        
        char *next_slash = strstr(after_slash, "/");
        
        /* Extract the name to search for this round. */
        char *name_to_search;
        int len;
        /* If has no next slash */
        if (next_slash == NULL) {
            name_to_search = after_slash;
            len = strlen(name_to_search);
        } else {
            
            /* Select the char sequence between two slashes. */
            len = next_slash - after_slash;
            name_to_search = calloc(len+1, sizeof(char));
            mem_test(name_to_search);
            
            strncpy(name_to_search, after_slash, len);
            name_to_search[len] = '\0';
        }
        
        
        vfat_seek_to_cluster(from_cluster);
        size_t cluster_size =
            ((uint32_t) vfat_info.boot_sector.sectors_per_cluster) *
            ((uint32_t) vfat_info.boot_sector.bytes_per_sector);
        
        /* Iterates over the direntry. */
        int i = 0;
        struct fat32_direntry direntry;
        for (i = read(vfat_info.fs, &direntry, sizeof(direntry));
             direntry.name[0] != (uint8_t) 0x00;
             i += read(vfat_info.fs, &direntry, sizeof(direntry)))
        {
            
            if (VFAT_IS_LONG_DIRENTRY(direntry)) {
                long_name_buffer = bbuffer_push(long_name_buffer, &direntry,
                                                sizeof(struct fat32_direntry_long));
            }
            else if (VFAT_IS_SHORT_DIRENTRY(direntry) && !VFAT_IS_VOLUME(direntry))
            {
                /* Calls the routine to parse the name (long or short). */
                
                if (!bbuffer_is_empty(long_name_buffer)) {
                    name_dst = calloc(bbuffer_bytes(long_name_buffer), sizeof(char));
                    mem_test(name_dst);
                } else {
                    name_dst = calloc(12, sizeof(char));
                    mem_test(name_dst);
                }
                
                if (vfat_getname(name_dst, &direntry, long_name_buffer, i / sizeof(direntry))) {
                    if (strcmp(name_to_search, name_dst) == 0) {
                        
                        if (next_slash == NULL) {
                            lseek(vfat_info.fs, -32, SEEK_CUR);
                            free(name_dst);
                            return 1;
                        } else {
                            uint32_t next_cluster = VFAT_CLUSTER(direntry);
                            free(name_dst);
                            free(name_to_search);
                            return _vfat_seek_to_path(next_slash, next_cluster);
                        }
                    }
                }
                
                long_name_buffer = NULL;
                free(name_dst);
            }
            
            if (i >= cluster_size) {
                uint32_t next_cluster = vfat_next_cluster(from_cluster);
                
                if (!vfat_is_valid_cluster(next_cluster)) {
                    if (next_slash) {
                        free(name_to_search);
                    }
                    return 0;
                }
                vfat_seek_to_cluster(next_cluster);
                from_cluster = next_cluster;
                i = 0;
            }
            
            
        }
        
        if (next_slash) {
            free(name_to_search);
        }
        
    }
    
    return 0;
    
}

static int vfat_seek_to_path(const char *path)
{
    if (strcmp(path, "/") == 0) {
        vfat_seek_to_cluster(vfat_info.boot_sector.fat32.root_cluster);
        return 1;
    } else {
        return _vfat_seek_to_path(path, vfat_info.boot_sector.fat32.root_cluster);
    }
}

/** Gets file attributes. */
static int vfat_fuse_getattr(const char *path, struct stat *st)
{
    /* Seeks to the entry described by path. */
    int found = vfat_seek_to_path(path);
    if (!found) {
        return -ENOENT;
    }
    
    /* Reads the diectory entry of the file. */
    struct fat32_direntry direntry;
    read(vfat_info.fs, &direntry, sizeof(direntry));
    
    vfat_parse_attr(&direntry, st);
    
    /* Computes the size of the directories if necessary. */
    if (VFAT_IS_DIR(direntry)) {
        
        uint32_t start_clus = VFAT_CLUSTER(direntry);
        int i = 1;
        
        while (vfat_is_valid_cluster(start_clus)) {
            ++i;
            start_clus = vfat_next_cluster(start_clus);
        }
        
        size_t cluster_size =
        ((uint32_t) vfat_info.boot_sector.sectors_per_cluster) *
        ((uint32_t) vfat_info.boot_sector.bytes_per_sector);
        
        st->st_size = i * cluster_size;
    }
    
    st->st_uid = mount_uid;
    st->st_gid = mount_gid;
    
    return 0;
}

/** Reads the content of a directory. Routine called by FUSE. */
static int vfat_fuse_readdir(const char *path, void *buf,
                  fuse_fill_dir_t filler, off_t offs, struct fuse_file_info *fi)
{
    /* Seek to the direntry in the appropriate cluster. */
    int found = vfat_seek_to_path(path);
    if (!found) return -ENOENT;
    
    uint32_t cluster;
    if (strcmp(path, "/") != 0) {
        struct fat32_direntry direntry;
        read(vfat_info.fs, &direntry, sizeof(direntry));
        cluster = VFAT_CLUSTER(direntry);
    } else {
        cluster = vfat_info.boot_sector.fat32.root_cluster;
    }
    
    vfat_readdir(cluster, filler, buf);
    return 0;
}

static int vfat_read(uint32_t from_cluster, char *buf,  size_t size, off_t offs)
{
    if (size == 0 || !vfat_is_valid_cluster(from_cluster)) return 0;
    size_t cluster_size =
        ((uint32_t) vfat_info.boot_sector.sectors_per_cluster) *
        ((uint32_t) vfat_info.boot_sector.bytes_per_sector);

    vfat_seek_to_cluster(from_cluster);
    
    /* If one reads just in the first cluster. */
    if (size + offs <= cluster_size) {

        /* Places the head of the disk at the right place. */
        lseek(vfat_info.fs, offs, SEEK_CUR);

        /* Reads and returns. */
        return read(vfat_info.fs, buf, size);
    }
    /* If one reads starting from another cluster or through several clusters. */
    else {
        
        while (offs >= cluster_size) {
            uint32_t next_cluster = vfat_next_cluster(from_cluster);
            if (!vfat_is_valid_cluster(next_cluster)) {
                memset(buf, 0, size);
                return 0;
            } else {
                vfat_seek_to_cluster(next_cluster);
                from_cluster = next_cluster;
                offs -= cluster_size;
            }
        }
        
        if (offs > 0) lseek(vfat_info.fs, offs, SEEK_CUR);
        
        /* Now we are at the right place to start reading. */
        
        int read_count = 0;
        size_t size_to_read = size;
        size_t original_size = size;
        
        while (read_count < original_size) {
            while (size_to_read > cluster_size) {
                size_to_read -= cluster_size;
            }
            
            if (offs > 0) {
                size_to_read -= offs;
                offs = 0;
            }
            
            int read_inc = read(vfat_info.fs, buf, size_to_read);
            
            read_count += read_inc;
            if (read_inc != size_to_read) {
                memset(buf, 0, original_size - read_count);
                return read_count;
            }
            
            
            buf += size_to_read;
            size -= size_to_read;
            size_to_read = size;
            
            /* Goes to the next cluster. */
            uint32_t next_cluster = vfat_next_cluster(from_cluster);
            
            /* If we encounter the end of the file. */
            if (!vfat_is_valid_cluster(next_cluster)) {
                if (read_count < original_size) {
                    memset(buf, 0, original_size - read_count);
                }
                return read_count;
            } else {
                vfat_seek_to_cluster(next_cluster);
                from_cluster = next_cluster;
            }
        }
        
        return read_count;
    }
}

static int vfat_fuse_read(const char *path, char *buf, size_t size, off_t offs,
                          struct fuse_file_info *fi)
{
    if (vfat_seek_to_path(path)) {
        struct fat32_direntry direntry;
        read(vfat_info.fs, &direntry, sizeof(direntry));
        
        uint32_t fcluster = VFAT_CLUSTER(direntry);
        return vfat_read(fcluster, buf, size, offs);
    } else return -1;
}

////////////// No need to modify anything below this point
static int vfat_opt_args(void *data, const char *arg, int key, struct fuse_args *oargs)
{
    if (key == FUSE_OPT_KEY_NONOPT && !vfat_info.dev) {
        vfat_info.dev = strdup(arg);
        return (0);
    }
    return (1);
}

static struct fuse_operations vfat_available_ops = {
    .getattr = vfat_fuse_getattr,
    .readdir = vfat_fuse_readdir,
    .read = vfat_fuse_read,
};

int main(int argc, char **argv)
{
	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

	fuse_opt_parse(&args, NULL, NULL, vfat_opt_args);

	if (!vfat_info.dev)
		errx(1, "missing file system parameter");
    
    /* inits and launches the filesystem. */
	vfat_init(vfat_info.dev);
	return (fuse_main(args.argc, args.argv, &vfat_available_ops, NULL));
}
