#include "vfat_util.h"
#include <time.h>

/** Chech whether a DIR_name is valid according to spec */
int vfat_is_dirname_valid(char *dirname, int N)
{
    uint8_t first_byte = dirname[0];
    int i;

    if (first_byte == 0xE5) {
        return 0;
    }

    for (i = 0; i < N; ++i) {
        uint8_t curr_byte = dirname[i];
        if (curr_byte == 0x22 || curr_byte == 0x2A || curr_byte == 0x2B ||
            curr_byte == 0x2C || curr_byte == 0x2E || curr_byte == 0x2F ||
            curr_byte == 0x3A || curr_byte == 0x3B || curr_byte == 0x3C ||
            curr_byte == 0x3D || curr_byte == 0x3E || curr_byte == 0x3F ||
            curr_byte == 0x5B || curr_byte == 0x5C || curr_byte == 0x5D ||
            curr_byte == 0x7C)
        {
            return 0;
        }

        if (first_byte != 0x05) {
            if (curr_byte < 0x20) {
                return 0;
            }
        }
    }

    return 1;
}

static time_t vfat_convert_time(uint16_t date_field, uint16_t time_field)
{
    time_t t;
    time(&t);
    
    struct tm *tinfo;
    tinfo = localtime(&t);
    
    tinfo->tm_year = (date_field >> 9) + 80;
    tinfo->tm_mon = ((date_field & 0x1ff) >> 5) - 1;
    tinfo->tm_mday = (date_field & 0x1f);
    tinfo->tm_hour = (time_field >> 11);
    tinfo->tm_min = ((time_field & 0x07ff) >> 5);
    tinfo->tm_sec = ((time_field & 0x1f) << 1);
    
    return mktime(tinfo);
}


void vfat_parse_attr(struct fat32_direntry *direntry, struct stat *st)
{
    st->st_dev = 0;
    st->st_ino = 0;
    
    
    if (VFAT_IS_DIR(*direntry) || VFAT_IS_VOLUME(*direntry)) {
        st->st_mode = S_IFDIR;
    } else {
        st->st_mode = S_IFREG;
    }
    
    if ((direntry->attr & VFAT_ATTR_READ_ONLY) > 0) {
        st->st_mode |= S_IRUSR | S_IRGRP | S_IROTH;
    } else {
        st->st_mode |= S_IRWXU | S_IRWXG | S_IRWXO;
    }
    
    st->st_nlink = 1;
    st->st_rdev = 0;
    st->st_blksize = 0;
    st->st_blocks = 1;
    st->st_size = direntry->size;
    st->st_atime = vfat_convert_time(direntry->atime_date, 0);
    st->st_mtime = vfat_convert_time(direntry->mtime_date, direntry->mtime_time);
    st->st_ctime = vfat_convert_time(direntry->ctime_date, direntry->ctime_time);
}


unsigned char checksum(struct fat32_direntry *direntry)
{
    short name_len;
    unsigned char sum = 0;
    
    char *fn = direntry->nameext;
    
    for (name_len = 11; name_len != 0; --name_len) {
        sum = ((sum & 1) ? 0x80 : 0) + (sum >> 1) + *fn;
        ++fn;
    }
    
    return sum;
}
