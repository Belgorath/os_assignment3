#include "bytebuffer.h"
#include <stdio.h>
#include <string.h>

static void bb_err(const char *msg)
{
    fputs(msg, stderr);
    exit(1);
}

size_t bbuffer_max_bytes(bytebuffer_t *buf)
{
    if (buf == NULL) return 0;
    
    size_t max = 0;
    
    do {
        if (buf->n_bytes > max) max = buf->n_bytes;
    } while ((buf = buf->next));
    
    return max;
}

size_t bbuffer_bytes(bytebuffer_t *buf)
{
    if (buf == NULL) return 0;
    return buf->n_bytes + bbuffer_bytes(buf->next);
}

bytebuffer_t *bbuffer_push(bytebuffer_t *buf, void *data, size_t n_bytes)
{
    bytebuffer_t *new_node = malloc(sizeof(bytebuffer_t));
    if (new_node == NULL) bb_err("bbuffer_append new_node malloc error.");
    
    byte_t *data_cpy = NULL;
    if (n_bytes > 0) {
        data_cpy = calloc(n_bytes, sizeof(byte_t));
        if (data_cpy == NULL) bb_err("bbuffer_append data calloc error.");
        memcpy(data_cpy, data, n_bytes);
    }
    
    new_node->data = data_cpy;
    new_node->n_bytes = n_bytes;
    new_node->next = buf;
    return new_node;
}



bytebuffer_t *bbuffer_pop(bytebuffer_t *buf, void *dst)
{
    if (buf) {
        if (dst) {
            memcpy(dst, buf->data, buf->n_bytes);
        }
        
        if (buf->data) free(buf->data);
        
        bytebuffer_t *res = buf->next;
        free(buf);
        return res;
    }
    
    return NULL;
}