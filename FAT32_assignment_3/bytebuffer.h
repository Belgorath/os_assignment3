#ifndef __BYTEBUFFER_H__
#define __BYTEBUFFER_H__

#include <stdlib.h>
#include <stdint.h>

typedef uint8_t byte_t;

/** A byte buffer is a dynamic array of byte implemented as a linked list. **/
typedef struct bytebuffer_t {
    byte_t *data;
    size_t n_bytes;
    struct bytebuffer_t *next;
} bytebuffer_t;

/** Returns the max n_bytes of the list. */
size_t bbuffer_max_bytes(bytebuffer_t *buf);

/** Returns the sum of all bytes contained in the structure. */
size_t bbuffer_bytes(bytebuffer_t *buf);

/** Appends n_bytes starting at data to the buffer buf. If buf = NULL,
 *  it creates a buffer */
bytebuffer_t *bbuffer_push(bytebuffer_t *buf, void *data, size_t n_bytes);

/** Removes the head of buf by writing it to dst (make sure dst has enough space).
 *  Returns the resulting buffer. */
bytebuffer_t *bbuffer_pop(bytebuffer_t *buf, void *dst);

#define bbuffer_is_empty(buf) (!(buf))
#define bbuffer_flush(buf) while ((buf = bbuffer_pop(buf, NULL))){}
#endif
